const { readSync } = require('fs');
const http = require('http');
const PORT = 4000;

const server = http.createServer((request, response) => {
    if (request.url === '/') {

        /* welcome to boooking system */
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end('Welcome to Booking System!');

    } else if (request.url === '/profile' && request.method === 'GET') {

        /* profile */
        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end('Welcome to your profile!')
    } else if (request.url === '/courses' && request.method === 'GET') {

        /* courses */
        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end(`Here's our courses available`)
    } else if (request.url === '/addcourse' && request.method === 'POST') {

        /* addcourse */
        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end('Add a course to our resources')

    } else if (request.url === '/updatecourse' && request.method === 'PUT') {

        /* updatecourse */
        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end('Update a course to our resources')
    } else if (request.url === '/archivecourses' && request.method === 'DELETE') {

        /* archivecourse */
        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end('Archive courses to our resources')
    } else {
        response.writeHead(404, { 'Content-Type': 'text/plain' });
        response.end('Error 404: Page not found.');
    }
})

server.listen(PORT);

console.log(`Server is running at localhost:${PORT}`);