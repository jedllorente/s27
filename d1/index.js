/* HTTP Methods
    HTTP methods of the incoming request can be accessed via the "method" property of the request parameter.

    GET - method for a request that indicates that we want to retrieve data.

    POST - method for a request that indicates that we want to add or send data to the server for processing. It is usually used for creating new entriesor documents.

    PUT - method for a request that indicates that we want to edit or update.

    DELETE - method for a request that indicates that we want to delete a specified document.

 */

const { readSync } = require('fs');
const http = require('http');
const PORT = 4000;

let directory = [{
        "name": "John",
        "email": "john@email.com"
    },
    {
        "name": "Jane",
        "email": "jane@email.com"
    }
]

const server = http.createServer((req, res) => {
    if (req.url === "/") {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('New Server');

    } else if (req.url === "/users" && req.method === "GET") {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify(directory));

    } else if (req.url === "/users" && req.method === "POST") {
        let requestBody = ' ';

        req.on('data', (data) => {
            requestBody += data;
        })

        req.on('end', () => {
            requestBody = JSON.parse(requestBody);
            let newUser = {
                name: requestBody.name,
                email: requestBody.email
            }
            directory.push(newUser);
            console.log(directory);

            res.writeHead(200, { 'Content-Type': 'application/json' })
            res.end(JSON.stringify(newUser))
        })
    } else if (req.url === '/users' && req.method === "DELETE") {
        directory.pop();

        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end('Item deleted.')

        console.log(directory);
    } else if (req.url === '/users' && req.method === 'PUT') {
        let requestBody = ' ';

        req.on('data', (data) => {
            requestBody += data;
        });

        req.on('end', () => {
            requestBody = JSON.parse(requestBody);
            directory[requestBody.index] = {
                name: requestBody.name,
                email: requestBody.email
            }

            console.log(directory[requestBody.index]);

            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify(directory[requestBody.index]));
        })

    }
})

server.listen(PORT);

console.log(`Server is running at localhost ${PORT}`);